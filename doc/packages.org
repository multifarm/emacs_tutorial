#+date: 2021
#+options: toc:nil num:nil author:nil

* How to organize, update and deal with 'packages' in emacs

** How to update packages:

Open emacs and then:

- Go to package's list using
   : Mx - package-list-packages
- Within the 'packages'  buffer use "U" to mark the packages to be upgraded
- Use "x" to execute the upgrade
- To delete old versions of packages use "autoremove"

: M-x package-autoremove

Or:

: M-x p-a

** Package management with helm

"Helm" is an alternative package to deal with packages in emacs.

: M-x helm-list-elisp-packages

| Keys  | Description                       |
|-------+-----------------------------------|
| M-A   | Show all packages.                |
| M-I   | Show installed packages only.     |
| M-O   | Show non-installed packages only. |
| M-U   | Show upgradable packages only.    |
| M-B   | Show built-in packages only.      |
| C-c i | Install package(s).               |
| C-c r | Reinstall package(s).             |
| C-c d | Uninstall package(s).             |
| C-c u | Upgrade package(s).               |
| C-c U | Upgrade all packages.             |
| C-c @ | Visit package homepage.           |

