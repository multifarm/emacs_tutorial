#+date: 2021
#+options: toc:nil num:nil author:nil

* Dired

Dired is a directory manager. You can find, open copy, move, delete query and
manipulate files and directories.

** Open a directory with dired and initialize mark regxexp:

: %m

Then, use regex to mark the files.

This will mark all the html files in directory

: \.html$

This includes all temporary files in dired

: $~

 This marks all files beginning with "_78"

: ^_78

** Delete, copy, move files or directories

- To delete, first flag the files ("d"). Then, use "x" to execute delete.
- To compare two files, mark the first file, then go to the other file and press "="
- To rename or move files first flag the file, then use "R"
- Mark all files within a directory (except '.' or '..'): ~*s~
- Unmark all marked files in directory: ~U~
- To copy files use "C" over the file you want to copy
- Convert files or directories to lower (~%l~) or upper case (~%u~)

** Tips

 * 'f11' toggle full screen mode the current frame
 * ibuffer kill multiple buffers
   * After open a list of buffers "M-x ibuffer" mark itens with "m"
and kill with "D"
 * Avoid ido auto complete to rename a file with similar name to those in directory
   * Once you've typed the filename you want, hit C-j instead of RET.
   * You can temporarily switch back to the "normal" minibuffer behaviour by hitting C-f.
     Then you can tab-complete the old filename, and edit it as needed.
   * From [[https://stackoverflow.com/questions/23741014/how-to-stop-auto-complete-from-overwriting-a-file-that-is-similarly-named][Stackoverflow]]
 * To use ~nano~ editor within emacs use:
   * 'CTRL + shift + o' instead of 'CTRL+o' to save changes
   *  ESC + ESC + x to close ~nano~

** Renaming multiple files in Dired

 * Use ~M-x wdired-change-to-wdired-mode~
 * Or (this is equivalent), use ~C-x C-q~ in a dired session.
 * Then you can edit all the files manually.
 * To escape from edition use ~C-c C-c~


** Substitute text in multiple files within a directory

